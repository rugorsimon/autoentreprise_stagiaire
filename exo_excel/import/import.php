<?php 
try {
	$PDO = new PDO('mysql:host=localhost;dbname=import_bdd_pays', 'root', '');
	$PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$PDO->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_OBJ);
} catch (PDOException $e) {
	print "Erreur !: " . $e->getMessage() . "<br/>";
	die();
}

if (isset($_FILES["file"]["type"]) != "application/vnd.ms-excel") {
	die("Ce n'est pas un fichier de type .csv");
} elseif (is_uploaded_file($_FILES['file']['tmp_name'])) {
	$crtb = $PDO->exec("CREATE TABLE IF NOT EXISTS import_pays 
		(
		id smallint(5) unsigned NOT NULL AUTO_INCREMENT,
		code int(3) NOT NULL,
		alpha2 varchar(2) NOT NULL,
		alpha3 varchar(3) NOT NULL,
		nom_en_gb varchar(45) NOT NULL,
		nom_fr_fr varchar(45) NOT NULL,
		PRIMARY KEY (id),
		UNIQUE KEY alpha2 (alpha2),
		UNIQUE KEY alpha3 (alpha3),
		UNIQUE KEY code_unique (code)
		)
		");
	$req = $PDO->prepare('INSERT INTO import_pays (id, code, alpha2, alpha3, nom_en_gb, nom_fr_fr) VALUES(?, ?, ?, ?, ?, ?)');
	$file = new SplFileObject($_FILES['file']['tmp_name']);
	$file->setFlags(SplFileObject::READ_CSV | SplFileObject::SKIP_EMPTY);
	$file->setCsvControl(';');
	var_dump($file);
	$flag = true;
	foreach ($file as $row) {
		if ($flag) { $flag = false; continue;}
		var_dump($row);
		var_dump($req);
		var_dump($file);
		$req->execute(array($row[0], $row[1], $row[2], $row[3], $row[4], $row[5]));
	}
	$req->closeCursor();
} else {
	echo 'Erreur <br />';
}
?>