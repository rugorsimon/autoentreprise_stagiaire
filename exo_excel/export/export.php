<?php 
header('Content-Type: text/csv;');
header('Content-Disposition: attachment; filename="Donnees_pays.csv"');
try {
    $PDO = new PDO('mysql:host=localhost;dbname=bdd_pays', 'root', '');
    $PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $PDO->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE,PDO::FETCH_OBJ);
} catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
}

$req = $PDO->prepare('SELECT * FROM pays');
$req->execute();
$data = $req->fetchAll();
// print_r($data);
?>"id";"code";"alpha2";"alpha3";"nom_en_gb";"nom_fr_fr"<?php
foreach ($data as $d) {
	echo "\n".'"'.$d->id.'";"'.$d->code.'";"'.$d->alpha2.'";"'.$d->alpha3.'";"'.$d->nom_en_gb.'";"'.$d->nom_fr_fr.'"';
}?>