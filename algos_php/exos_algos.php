<?php 


function Affichage_nb()
{
	$i = 0;
	$s = "";
	while ($i <= 10) {
		$s = $s . $i;
		$i++;
	}
	return($s);
}

function Affichage_somme_nb()
{
	$i = 0;
	$s = "";
	while ($i <= 10) {
		$s = $s + $i;
		$i++;
	}
	return($s);
}

function Affichage_nb_nm_v1($n, $m)
{
	$s = "";
	if ($m > $n) {
		$s = $n."|".$m;
	}
	else {
		$s ="Erreur n est supérieur à m";
	}
	return($s);
}

function Affichage_nb_nm_v2($n, $m)
{
	$s = "";
	if ($m > $n) {
		$s = $n."|".$m;
	}
	else {
		$v = $m;
		$m = $n;
		$n = $v;
		$s = $n."|".$m;
	}
	return($s);
}

function Affichage_nb_nm_v3($n, $m)
{
	$s = "";
	if ($m > $n) {
		$s = $n."|".$m;
	}
	else {
		while ($n > $m) {
			$s = $n."|";
			$n--;
		}
		$s = $s .$m;
	}
	return($s);
}

function Affichage_somme_nb_impairs_nm($n, $m)
{
	$somme = 0;
	if ($n >= $m) {
		while ($n >= $m) {
			if ($n%2 != 0){
				$somme = $somme + $n;
			}
			$n--;
		}
	}elseif ($n <= $m) {
		while ($n <= $m) {
			if ($n%2 != 0) {
				$somme = $somme + $n;
			}
			$n++;
		}
	}
	return($somme);
}

function nb_lePlusGrand($Nb)
{
	$g = 0;
	$s = "";
	for ($i=0; $i < 10 ; $i++) { 
		if ($Nb[$i] >= $g) {
			$g = $Nb[$i];
			$v = $i;
		}
	}
	$s = $g."|".$v;
	return($s);
}

function nb_lePlusPetit($Nb)
{
	$g = INF;
	$s = "";
	for ($i=0; $i < 10 ; $i++) { 
		if ($Nb[$i] <= $g) {
			$g = $Nb[$i];
			$v = $i;
		}
	}
	$s = $g."|".$v;
	return($s);
}

function Moy_n_nb($nb, $Notes)
{
	$moy = 0;
	$somme = 0;
	if ($nb == count($Notes)) {
		for ($i=0; $i < $nb; $i++) { 
			$somme = $somme + $Notes[$i];
		}
		$moy = $somme / $nb;
	}
	return($moy);
}

function rangsValeurs_nb_plus_pg($Nb)
{
	$s = "";
	$p = nb_lePlusPetit($Nb);
	$g = nb_lePlusGrand($Nb);
	$s = $p."//".$g;
	return($s);
}

function Affichage_infos_tab($Nb)
{
	$somme = 0;
	$r = "";
	$s = "";
	$g = nb_lePlusGrand($Nb);
	$m = Moy_n_nb(count($Nb), $Nb);
	for ($i=0; $i < 10; $i++) { 
		if ($Nb[$i] < $m) {
			$Inf[] = $Nb[$i];
		}
	}
	foreach ($Inf as $infer) {
		$s = $s . $infer.";";
	}
	$r = $g."//".$m."//".$s;
	return($r);
}

function inverser_tab($Nb)
{
	$i = 0;
	$j = 9;
	$x = 0;
	$s = "";
	while ($i < $j) {
		$x = $Nb[$j];
		$Nb[$j] = $Nb[$i];
		$Nb[$i] = $x;
		$i++;
		$j--;
	}
	foreach ($Nb as $valTab) {
		$s = $s.$valTab.";";
	}
	return($s);
}

function tri_ordre_croi($Nb)
{
	$x = 0;
	$s = "";
	for ($i=0; $i < 10; $i++) { 
		for ($j=0; $i + 1 < 11 and $j < 10; $j++) { 
			if ($Nb[$j] > $Nb[$i]) {
				$x = $Nb[$i];
				$Nb[$i] = $Nb[$j];
				$Nb[$j] = $x;			
			}
		}
	}
	foreach ($Nb as $valTab) {
		$s = $s.$valTab.";";
	}
	return($s);

	//Ou tout simplement faire sort($Nb);
}

function fusion_tab($Nb1, $Nb2)
{
	$s = "";
	sort($Nb1);
	sort($Nb2);

	$Nb3 = array_merge($Nb1, $Nb2);
	sort($Nb3);

	foreach ($Nb3 as $valTab) {
		$s = $s.$valTab.";";
	}
	return($s);
}

function verif_val($x, $Nb)
{
	$trouve = false;
	$debut = 0;
	$fin = count($Nb) - 1;
	$milieu = 0;

	while ($trouve == false and $debut <= $fin) {
		$milieu = ($debut + $fin) / 2;
		if ($Nb[$milieu] == $x) {
			$trouve = true;
		}
		elseif ($Nb[$milieu] < $x) {
			$debut = $milieu + 1;
		}
		else
		{
			$fin = $milieu - 1;
		}
	}
	return(var_export($trouve));
}

function cinquante_tirets()
{
	$texte = "";
	for ($i=0; $i < 50 ; $i++) { 
		$texte = $texte . "-";
	}
	return($texte);
}

function inverser_ch($ch)
{
	$ch2 = "";
	$i = strlen($ch);
	while ($i > 0) {
		$ch2 = $ch2 . $ch[$i - 1];
		$i--;
	}
	return($ch2);
}

function remplacer_etoile($ch)
{
	for ($i=0; $i < strlen($ch); $i++) { 
		$ch[$i] = "*";
	}
	return($ch);
}

function remplacer_toutes_lettres($ch, $c, $c2)
{
	for ($i=0; $i < strlen($ch); $i++) { 
		if ($ch[$i] == $c) {
			$ch[$i] = $c2;
		}
	}
	return($ch);
}

function supprimer_toutes_lettres($ch, $c)
{
	for ($i=0; $i < strlen($ch); $i++) { 
		if ($ch[$i] == $c) {
			$ch[$i] = '';
		}
	}
	return($ch);
}

function presence_lettre($ch, $c)
{
	$compteur = 0;
	for ($i=0; $i < strlen($ch); $i++) { 
		if ($ch[$i] == $c) {
			$compteur++;
		}
		else
		{
			$compteur = "Absent";
		}
	}
	return($compteur);
}

function nb_lettre($ch)
{
	$c = '';
	$s = "";
	$compteur = 0;

	$chtab = str_split($ch);
	for ($i=0; $i < strlen($ch); $i++) { 
		foreach ($chtab as $c) {
			$compteur = substr_count($ch, $chtab[$i]);
		}
		$s = $s . $chtab[$i] . " apparraît " . $compteur . " fois." . "\n";
	}
	return($s);
}

function supp_plus_dun_esp($ch)
{
	$ch = preg_replace('/\s\s+/', ' ', $ch);
	return($ch);
}

function decoupage_comptage_ch($ch)
{
	$chtab = explode(' ', $ch);
	$nb = count($chtab);
	return($nb);
}

function inverser_mots_ch($ch)
{
	$i = 0;
	$chtab = explode(' ', $ch);
	$j = count($chtab) - 1;
	$x = "";
	$s = "";
	while ($i < $j) {
		$x = $chtab[$j];
		$chtab[$j] = $chtab[$i];
		$chtab[$i] = $x;
		$i++;
		$j--;
	}
	foreach ($chtab as $valTab) {
		$s = $s.$valTab." ";
	}
	return($s);
}

?>