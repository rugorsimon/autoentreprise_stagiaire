<?php

namespace App\Controllers;

use \Core\View;
use \App\Auth;
use \App\Models\Portfolio;
/**
 * Home controller
 *
 * PHP version 5.4
 */
class Home extends \Core\Controller
{

    /*
     * Controlleur d'affichage de la page d'accueil
     */
    public function indexAction()
    {
        $titre_onglet = Portfolio::getTitreOnglet();
        $titre = Portfolio::getHomeTitre();
        $footer = Portfolio::getFooter();

        View::renderTemplate('Home/index.html', [
            'titre_onglet' => $titre_onglet,
            'titre' => $titre,
            'footer' => $footer
        ]);
    }

    public function experiencesAction()
    {
        $titre_onglet = Portfolio::getTitreOnglet();
        $data = Portfolio::getExperiences();
        View::renderTemplate('Experiences/index.html', [
            'titre_onglet' => $titre_onglet,
            'data' => $data
        ]);
    }

    public function competencesAction()
    {
        $langages = Portfolio::getCompetences("Languages");
       // var_dump($langages);
             View::renderTemplate('Competences/index.html', [
            'langages' => $langages
        ]);
    }

    public function etudesAction()
    {
        View::renderTemplate('Etudes/index.html');
    }


}
