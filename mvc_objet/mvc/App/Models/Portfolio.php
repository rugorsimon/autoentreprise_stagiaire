<?php

namespace App\Models;

use PDO;

/**
 * Post model
 *
 * PHP version 5.4
 */
class Portfolio extends \Core\Model
{

    /* Recupere le titre (onglet navigateur)*/
    public static function getTitreOnglet()
    {
        try {
            $db = static::getDB();

            $sql = 'SELECT * FROM texte_site where texte_site_id = 4';
            $db = static::getDB();
            $stmt = $db->prepare($sql);
            $stmt->execute();

            return $stmt->fetch();

        
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    /* Recupere le titre de la page d'accueil*/
    public static function getHomeTitre()
    {
        try {
            $db = static::getDB();

            $sql = 'SELECT * FROM texte_site where texte_site_id = 2';
            $db = static::getDB();
            $stmt = $db->prepare($sql);
            $stmt->execute();

            return $stmt->fetch();

        
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

     /* Recupere le footer*/
    public static function getFooter()
    {
        try {
            $db = static::getDB();

            $sql = 'SELECT * FROM texte_site where texte_site_id = 3';
            $db = static::getDB();
            $stmt = $db->prepare($sql);
            $stmt->execute();

            return $stmt->fetch();

        
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    /* Recupere les experiences pro*/
    public static function getExperiences()
    {
        try {
            $db = static::getDB();

            $sql = 'SELECT * FROM experiences_pro ORDER BY date_fin_projet DESC';
            $db = static::getDB();
            $stmt = $db->prepare($sql);
            $stmt->execute();

            return $stmt->fetchAll();

        
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    /* Recupere les experiences pro*/
    public static function getCompetences($categorie)
    {
        try {
            $db = static::getDB();

            $sql = "SELECT * FROM competences WHERE categorie_competence = :categorie ORDER BY id ASC";
            $db = static::getDB();
            $stmt = $db->prepare($sql);
            $stmt->bindValue(':categorie', $categorie);
            $stmt->execute();

            return $stmt->fetchAll();

        
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }













}
