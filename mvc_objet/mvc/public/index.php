<?php

/**
 * Front controller
 *
 * PHP version 7.0
 */

ini_set('session.cookie_lifetime', '864000'); // ten days in seconds

/**
 * Composer
 */
require dirname(__DIR__) . '/vendor/autoload.php';

/**
 * Error and Exception handling
 */

// Activer ou desactiver les erreurs PHP
// Custom Simon
ini_set('display_errors', 1);
// Enregistrer les erreurs dans un fichier de log
ini_set('log_errors', 1);
error_reporting(E_ALL);

/* A reactiver d'origine si besoin
error_reporting(E_ALL);
set_error_handler('Core\Error::errorHandler');
set_exception_handler('Core\Error::exceptionHandler');
*/

/**
 * Sessions
 */
session_start();

/**
 * Routing
 */
$router = new Core\Router();





/******************** GESTION DES ROUTES DU PORTFOLIO ********************/
// Affiche la page d'accueil
$router->add('accueil', ['controller' => 'Home', 'action' => 'index']);

// Affiche la page experience
$router->add('experiences', ['controller' => 'Home', 'action' => 'experiences']);

// Affiche la page experience
$router->add('competences', ['controller' => 'Home', 'action' => 'competences']);

// Affiche la page etudes
$router->add('etudes', ['controller' => 'Home', 'action' => 'etudes']);

// Affiche le back-office lors de l'identification
$router->add('admin', ['controller' => 'Admin', 'action' => 'admin']);




$router->add('{controller}/{action}');
$router->add('{controller}/{id}/{action}');
$router->add('{controller}/{id:\d+}/{action}');
$router->add('admin/{controller}/{action}', ['namespace' => 'Admin']);

$router->dispatch($_SERVER['QUERY_STRING']);