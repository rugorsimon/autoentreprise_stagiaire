-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Client: sql208.byethost.com
-- Généré le: Sam 01 Décembre 2018 à 14:08
-- Version du serveur: 5.6.41-84.1
-- Version de PHP: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `b9_19502061_portfolio`
--

-- --------------------------------------------------------

--
-- Structure de la table `competences`
--

CREATE TABLE IF NOT EXISTS `competences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `intitule_competence` varchar(255) NOT NULL,
  `niveau_competence` varchar(255) DEFAULT NULL,
  `categorie_competence` varchar(255) DEFAULT NULL,
  `pourcentage` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Contenu de la table `competences`
--

INSERT INTO `competences` (`id`, `intitule_competence`, `niveau_competence`, `categorie_competence`, `pourcentage`) VALUES
(1, 'PHP', 'Niveau comfirmé', 'Languages', 80),
(2, 'JAVA(Logiciel)', 'Niveau intermediaire', 'Languages', 50),
(3, 'JAVA(Android)', 'Niveau intermediaire', 'Languages', 50),
(4, 'SQL\r\n', 'Niveau comfirmé', 'Languages', 80),
(5, 'HTML5', 'Niveau comfirmé', 'Languages', 80),
(6, 'CSS3', 'Niveau comfrmé', 'Languages', 80),
(7, 'JAVASCRIPT\r\n', 'Niveau intermediaire', 'Languages', 50),
(8, 'NODEJS', 'Niveau intermediaire', 'Languages', 60);

-- --------------------------------------------------------

--
-- Structure de la table `experiences_pro`
--

CREATE TABLE IF NOT EXISTS `experiences_pro` (
  `id` int(11) NOT NULL,
  `nom_entreprise` varchar(64) DEFAULT NULL,
  `nom_poste` varchar(150) DEFAULT NULL,
  `nom_projet` varchar(150) DEFAULT NULL,
  `date_debut_projet` date DEFAULT NULL,
  `date_fin_projet` date DEFAULT NULL,
  `lien_site` varchar(200) DEFAULT NULL,
  `description_mission` text,
  `lieux` varchar(100) DEFAULT NULL,
  `image_experience` varchar(200) DEFAULT NULL,
  `fiche_descriptif` varchar(255) DEFAULT NULL,
  `langages` varchar(250) DEFAULT NULL,
  `outils` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `experiences_pro`
--

INSERT INTO `experiences_pro` (`id`, `nom_entreprise`, `nom_poste`, `nom_projet`, `date_debut_projet`, `date_fin_projet`, `lien_site`, `description_mission`, `lieux`, `image_experience`, `fiche_descriptif`, `langages`, `outils`) VALUES
(1, 'Cyrtech Conseil', 'Développeur Visual Basic', 'Parseur Societe Generale', '2016-06-05', '2016-07-29', NULL, 'Poste de développeur Web pour la création d''un site appartenant à la société générale', 'Montfermeil 93370', 'public/src/img/experience.png', 'public/fiche_descriptif/cyrtech_conseil.pdf', 'Visual Basic', 'Visual Studio Code'),
(2, 'Michelange CONSULTING', 'Développeur Web', 'Optimisations des sites', '2017-01-09', '2017-03-31', 'https://www.michelange.consulting/', '/', 'Gonesse 95500', 'public/src/img/experience.png', 'public/fiche_descriptif/michelange.pdf', 'PHP | MySQL', 'PrestaShop | Wordpress'),
(3, 'Antilock', 'Webmaster', 'Optimisations et améliorations du site ', '2017-04-03', '2017-04-28', 'https://www.antilock.fr/', '/', 'Paris 11', 'public/src/img/experience.png', 'public/fiche_descriptif/antilock.pdf', 'PHP', 'Prestashop'),
(4, 'ACENSI SS2I', 'Développeur Web', 'Créations et optimisations de solutions', '2017-07-03', '2019-09-10', 'https://acensi.fr/', '/', 'La Defense 92000', 'public/src/img/experience.png', 'public/fiche_descriptif/acensi.pdf', 'PHP | SQL | JAVASCRIPT | TWIG ', 'Symfony | Drupal | API REST | Postgres | Sourcetree'),
(5, 'Independant', 'Développeur PHP ', 'Portfolio', '2018-03-01', '2018-03-31', NULL, '/', 'Montfermeil 93370', 'public/src/img/experience.png', 'public/fiche_descriptif/portfolio.pdf', 'PHP | SQL | TWIG | JAVASCRIPT', 'BOOTSTRAP '),
(6, 'Independant', 'Développeur NodeJS / PHP', 'Jeu multijoueur', '2018-03-10', '2018-06-12', NULL, '/', 'Montfermeil 93370', 'public/src/img/experience.png', 'public/fiche_descriptif/node.pdf', 'NODEJS | JAVASCRIPT | PHP', 'SOCKET.IO'),
(7, 'Independant', 'Chef de projet & développeur mobile', 'Plateforme de recherche d''emplois', '2018-03-03', '2019-02-10', NULL, '/', 'Montfermeil 93370', 'public/src/img/experience.png', 'public/fiche_descriptif/emploi_finder.pdf', 'JAVASCRIPT | PHP | HTML | CSS', 'CORDOVA | IONIC ');

-- --------------------------------------------------------

--
-- Structure de la table `texte_site`
--

CREATE TABLE IF NOT EXISTS `texte_site` (
  `texte_site_id` int(3) NOT NULL AUTO_INCREMENT,
  `texte_site_texte` varchar(255) NOT NULL,
  `type` varchar(50) NOT NULL,
  PRIMARY KEY (`texte_site_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `texte_site`
--

INSERT INTO `texte_site` (`texte_site_id`, `texte_site_texte`, `type`) VALUES
(1, 'BIENVENUE SUR MON PORTFOLIO', 'homepage_titre'),
(2, 'Je m’appelle Simon RUGOR, j’ai 21 ans j’habite à Montfermeil et je suis actuellement en 3ème année de Licence professionnelle Parcours développement Web et Mobile en apprentissage.', 'homepage_description'),
(3, 'Vous avez des questions ? Me contacter via les informations ci-dessous ! <br>\r\n            Tél: 06 52 57 54 52 <br>\r\n            Mail : rugor.simon@yahoo.com', 'footer_contact'),
(4, 'RUGOR Simon - Portfolio', 'titre_onglet');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
